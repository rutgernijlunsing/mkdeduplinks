﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MkDedupLinks {
    /// <summary>Parsing of command line options</summary>
    public class Config {
        public bool DoLink = false;
        public long MinFileSize = 4096;
        public long MaxFileSize = long.MaxValue;
        public bool Incremental = false;

        public List<string> RootDirs = new();

        private static void Usage() {
            Console.WriteLine(@"MkDedupLink

Usage:
    MkDedupLink [options] [directory(s)]

Options (with defaults in brackets):
    /link                 Perform Hard-linking. Otherwise only show what could be hard linked
    /min SIZE             Minimal size in bytes before considering a file
    /max SIZE             Maximum size in bytes before considering a file
    /size SIZE            Exact size in bytes before considering a file (for debugging)
    /i                    Incremental. Consider files from previous runs on same volume to also link to.
    /? /h                 This page
");
        }

        private void ParseOpts(string[] args) {
            for (int i = 0; i < args.Length; i++) {
                string argl = args[i].ToLowerInvariant();
                if (argl.StartsWith("/")) {
                    bool v = true;
                    argl = argl[1..];
                    if (argl.StartsWith("no", StringComparison.OrdinalIgnoreCase)) {
                        argl = argl[2..];
                        v = false;
                    }
                    switch (argl) {
                        case "link": DoLink = v; break;
                        case "min": MinFileSize = long.Parse(args[++i]); break;
                        case "max": MaxFileSize = long.Parse(args[++i]); break;
                        case "size": MaxFileSize = MinFileSize = long.Parse(args[++i]); break;
                        case "i": case "incremental": Incremental = v; break;
                        case "usage": case "?": case "h":
                            Usage();
                            Environment.Exit(0);
                            break;
                        default:
                            Usage();
                            string err = $"! Unknown parameter '{args[i]}'";
                            Console.WriteLine(err);
                            Environment.Exit(1);
                            break;
                    }
                } else {
                    string rootDir = Path.GetFullPath(args[i]);
                    while (!rootDir.EndsWith(@":\") && rootDir.EndsWith(@"\")) {
                        rootDir = rootDir[..^1];
                    }
                    RootDirs.Add(rootDir);
                }
            }

            if (RootDirs.Count == 0) {
                Usage();
                const string err = "! Missing directory to process";
                Console.WriteLine(err);
                Environment.Exit(1);
            }
        }

        static public Config Parse(string[] args) {
            var res = new Config();
            res.ParseOpts(args);
            return res;
        }
    }
}
