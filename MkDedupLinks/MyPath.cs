﻿using System.Text;

namespace MkDedupLinks {
    /// <summary>Simplified System.IO.Path methods which do not throw exceptions on invalid characters or too long paths</summary>
    public static class MyPath {
        /// <summary>Provides a platform-specific character used to separate directory levels in a path string that reflects a hierarchical file system organization.</summary>
        public static readonly char DirectorySeparatorChar = '\\';
        /// <summary>Provides a platform-specific alternate character used to separate directory levels in a path string that reflects a hierarchical file system organization.</summary>
        public static readonly char AltDirectorySeparatorChar = '/';
        /// <summary>Provides a platform-specific volume separator character.</summary>
        public static readonly char VolumeSeparatorChar = ':';

        private static int GetLastIndexOfDirectorySeparator(string path) {
            int lastDirSepIdx = path.Length - 1;
            while (lastDirSepIdx >= 0) {
                char ch = path[lastDirSepIdx];
                if (ch == DirectorySeparatorChar || ch == AltDirectorySeparatorChar) {
                    return lastDirSepIdx; // Found
                }
                lastDirSepIdx -= 1;
            }
            // Not found
            return -1;
        }

        public static string GetDirectoryName(string path) {
            if (path == null) return null;
            int idx = GetLastIndexOfDirectorySeparator(path);
            return idx == -1 ? "" : path.Substring(0, idx);
        }

        public static string GetFileName(string path) {
            if (path == null) return null;
            int idx = GetLastIndexOfDirectorySeparator(path);
            return path[(idx + 1)..];
        }

        public static string GetFileNameWithoutExtension(string path) {
            if (path == null) return null;
            string filename = GetFileName(path);
            int idx = filename.LastIndexOf('.');
            if (idx == -1) return filename;
            return filename.Substring(0, idx);
        }

        /// <summary>Returns the extension of the specified path (including the period ".").
        /// If path does not have extension information, GetExtension(String) returns Empty.</summary>
        public static string GetExtension(string path) {
            if (path == null) return null;
            string filename = GetFileName(path);
            int idx = filename.LastIndexOf('.');
            if (idx == -1) return string.Empty;
            return filename[idx..];
        }

        public static string Combine(params string[] paths) {
            var res = new StringBuilder(256);
            foreach (var p in paths) {
                if (res.Length > 0) {
                    char lastChar = res[^1];
                    if (lastChar != DirectorySeparatorChar && lastChar != AltDirectorySeparatorChar) {
                        res.Append(DirectorySeparatorChar);
                    }
                }
                res.Append(p);
            }
            return res.ToString();
        }
    }
}