﻿using SqlDatabase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace MkDedupLinks {
    public class Statistics {
        public int NrOfFiles = 0;
        public int MaxNrOfFilesSameSize = 0;
        public int NrOfDirectories = 0;
        public int NrOfQuickChecksums = 0;
        public int NrOfChecksums = 0;

        public override string ToString() => $"NrOfFiles={NrOfFiles}; NrOfDirectories={NrOfDirectories}; NrOfQuickChecksums={NrOfQuickChecksums}; NrOfChecksums={NrOfChecksums}; MaxNrOfFilesSameSize={MaxNrOfFilesSameSize}";
    }

    public class Program {
        private Config conf;
        private readonly Statistics stats = new();
        private DatabaseConnection db;
        private readonly List<string> directories = new();

        /// <summary>File extension to use during creation of hardlinks</summary>
        private const string TmpFileExt = ".tmp.mkdeduplink";

        private static string SqlPath(uint volumeSerialNumber) {
            string dirsDirectory = Environment.GetEnvironmentVariable("APPDATA");
            dirsDirectory ??= Path.GetTempPath();
            return MyPath.Combine(dirsDirectory, $"MkDedupLinksCache-{volumeSerialNumber}.sqlite");
        }

        private void CreateSqlTables() {
            // Persistent cache about NTFS FileIds ('inodes'):
            db.ExecuteNonQuery(
                @"CREATE TABLE IF NOT EXISTS FileIds (
                file_id INTEGER NOT NULL PRIMARY KEY,
                version INTEGER NOT NULL,
                size INTEGER NOT NULL, 
                contents_hash BLOB
            );");

            db.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS FileIdSizeContentsHashIndex ON FileIds(contents_hash);");

            // Files of current run:
            db.ExecuteNonQuery("DROP TABLE IF EXISTS FileInfos");
            db.ExecuteNonQuery(
                @"CREATE TABLE IF NOT EXISTS FileInfos (
                filename TEXT NOT NULL,
                directory INTEGER NOT NULL,
                version INTEGER NOT NULL,
                size INTEGER NOT NULL
            );");

            db.ExecuteNonQuery("CREATE INDEX IF NOT EXISTS FilenInfoSizeIndex ON FileInfos(size);");

            // After dropping a table a lot might have been gone, so clean it up:
            db.Optimize();
        }

        public static bool HasExtension(string[] paths, params string[] exts) =>
            paths.All(path => exts.Any(ext => path.EndsWith(ext, StringComparison.InvariantCultureIgnoreCase)));

        public static bool HasExtension(string path, params string[] exts) => HasExtension(new[] { path }, exts);

        /// <summary>Tries operation. If exception, log this to output. Returns result of function if all OK, default otherwise</summary>
        private static T SafeOrDefault<T>(string actionMsg, Func<T> func) {
            try {
                return func.Invoke();
            } catch (Exception e) {
                Console.WriteLine($"! Exception while {actionMsg}:");
                Console.WriteLine($"! {e.Message}");
                return default;
            }
        }

        /// <summary>Tries operation. If exception, log this to output. Returns true if all OK, false otherwise</summary>
        private static bool Safe(string actionMsg, Action action) => SafeOrDefault(actionMsg, () => { action.Invoke(); return true; });

        private void IndexFilenames(string rootDir) {
            Console.WriteLine($". Indexing '{rootDir}'...");
            var toProcess = new Stack<string>(new[] { Path.GetFullPath(rootDir)});
            stats.NrOfFiles = 0;
            stats.NrOfDirectories = 0;
            while (toProcess.Count > 0) {
                string dir = toProcess.Pop();
                directories.Add(dir);
                int directoryIndex = directories.Count - 1;
                foreach (var fi_ in Win32Api.GetFileInfos(dir, includeDirectoryInPath: false)) {
                    var fi = fi_;
                    if (fi.IsReparsePoint) {
                        Console.WriteLine($". Skipping reparse point '{fi.Path}'");
                    } else if (fi.IsDirectory) {
                        string fullPath = MyPath.Combine(dir, fi.Path);
                        toProcess.Push(fullPath);
                        stats.NrOfDirectories += 1;
                    } else {
                        if (HasExtension(fi.Path, TmpFileExt)) {
                            // Previous time it crashed; fix it
                            string origPath = fi.Path[..^TmpFileExt.Length];
                            if (File.Exists(origPath)) {
                                if (!Safe($"deleting '{fi.Path}'; trying other...", () => File.Delete(fi.Path))) {
                                    // Could not delete one; try the other
                                    Safe($"deleting '{origPath}'; ignoring.", () => File.Delete(origPath));
                                }
                                // No clue about current state; so ignore this file for now...
                                continue;
                            } else {
                                File.Move(fi.Path, origPath);
                                fi = fi with { Path = origPath };
                            }
                        }
                        bool fileSizeOk = (long)fi.FileSize >= conf.MinFileSize && (long)fi.FileSize <= conf.MaxFileSize;
                        if (!fileSizeOk) {
                            continue;
                        }
                        int rows = db.ExecuteNonQuery(
                            "INSERT INTO FileInfos(version, size, filename, directory) VALUES (?, ?, ?, ?)",
                            new IntegerParameterValue(fi.Version),
                            new IntegerParameterValue((long)fi.FileSize),
                            new StringParameterValue(fi.Path),
                            new IntegerParameterValue(directoryIndex)
                        );
                        stats.NrOfFiles += 1;
                        Debug.Assert(rows == 1);
                        WriteProgress(() => $"#={stats.NrOfFiles}");
                    }
                }
            }
            Console.WriteLine($". Found {stats.NrOfFiles} files, {stats.NrOfDirectories} directories.");
        }

        private DateTime lastProgressUpdateAt = DateTime.MinValue;
        private void WriteProgress(Func<string> gen) {
            var now = DateTime.Now;
            double elapsed = (now - lastProgressUpdateAt).TotalSeconds;
            if (elapsed >= 1) {
                string line = $"[{gen.Invoke()}]".PadRight(79);
                Console.Error.Write(line);
                Console.Error.Write("".PadLeft(100, '\b'));  // backspaces to start of line
                lastProgressUpdateAt = now;
            }
        }

        private record FileInfoRecord(List<string> Directories, string Filename, int Directory, long Version, long FileSize) {
            public string Path => MyPath.Combine(Directories[Directory], Filename);
            public string Extension => MyPath.GetExtension(Filename);
        }

        private IEnumerable<List<FileInfoRecord>> ForAllFilesWithSameSize() {
            List<FileInfoRecord> sameSizeFileInfos = new();
            int nFilesDone = 0;
            foreach (var (filename, directory, version, size) in db.ExecuteRows<string, long, long, long>(
                // Select file with non-unique sizes, ordered by size:
                "SELECT filename, directory, version, size FROM FileInfos WHERE size IN (SELECT size FROM FileInfos GROUP BY size HAVING COUNT(*) > 1) ORDER BY size DESC",
                new IntegerParameterValue[0],
                new[] { "filename", "directory", "version", "size" })
            ) {
                nFilesDone += 1;
                var fi = new FileInfoRecord(directories, filename, (int)directory, version, size);
                bool newGroup = sameSizeFileInfos.Count > 0 && sameSizeFileInfos[0].FileSize != size;
                if (newGroup) {
                    yield return sameSizeFileInfos;
                    sameSizeFileInfos = new();
                }
                sameSizeFileInfos.Add(fi);
            }
            if (sameSizeFileInfos.Count > 0) yield return sameSizeFileInfos;
        }

        private byte[] CalculateQuickContentHash(long fileSize, string path) {
            WriteProgress(() => $"{fileSize}> {Path.GetFileName(path)}");
            stats.NrOfQuickChecksums += 1;
            return SafeOrDefault("Reading file", () => {
                using var stream = File.OpenRead(path);
                long offset = (long)(0.872346 * fileSize) & ~4095;
                if (offset > fileSize - 4096) offset = (fileSize & ~4095) - 4096;
                byte[] buffer = new byte[4096];
                stream.Seek(offset, SeekOrigin.Begin);
                int n = stream.Read(buffer, 0, 4096);
                return CalculateContentHash(new MemoryStream(buffer, 0, n));
            });
        }

        private byte[] CalculateContentHash(Stream stream) {
            using var sm = new SHA256Managed();
            return sm.ComputeHash(stream).Take(16).ToArray();
        }

        /// <summary>Returns hash of contents of complete file, or null in case of error</summary>
        private byte[] CalculateContentHash(string path) {
            stats.NrOfChecksums += 1;
            return SafeOrDefault("Reading file", () => {
                using var stream = File.OpenRead(path);
                return CalculateContentHash(stream);
            });
        }

        /// <summary>Tries to get contents hash from cache. If not in cache or cache not up-to-date, calculate value
        /// and update cache. Returns null in case of error.</summary>
        private byte[] GetOrCreateContentHash(FileInfoRecord fi) {
            WriteProgress(() => $"{fi.FileSize}: {fi.Filename}");
            string path = fi.Path;
            var pathInfo = Win32Api.GetPathInformation(path, isDirectory: false, ifInvalidHandle: IfInvalidHandle.ReturnNull);
            if (pathInfo == null) {
                return null;            // Error
            }

            bool sameContents = (pathInfo.Version == fi.Version) && (pathInfo.FileSize == (ulong)fi.FileSize);
            if (!sameContents) {
                // Strange but true: FileNextFile() returned different timestamp than GetPathInformation. Whatever...
                Console.WriteLine($"? '{path}': File timestamps not stable");
            }
            var fileId = (long)pathInfo.FileIndex;
            var row = db.ExecuteRows<long, long, byte[]>(
                "SELECT version, size, contents_hash FROM FileIds WHERE file_id = ?", 
                new[] { new IntegerParameterValue(fileId) },
                new[] { "version", "size", "contents_hash" }
            ).FirstOrDefault();

            // Check if cache is valid. If so, return the cache:
            if (row != null) {
                var (version, size, contentsHash2) = row;
                if (version == pathInfo.Version && (ulong)size == pathInfo.FileSize) {
                    return contentsHash2;
                }
            }

            // Cache is not valid, calculate the (new) value:
            byte[] contentsHash = CalculateContentHash(path);
            if (contentsHash == null) {
                return null;        // Error
            }

            // Insert or update value in cache, depending on whether it already exists:
            string query = row == null
                ? "INSERT INTO FileIds(version, size, contents_hash, file_id) VALUES (?, ?, ?, ?)"
                : "UPDATE FileIds SET version=?, size=?, contents_hash=? WHERE file_id=?";
            int rows = db.ExecuteNonQuery(query,
                new IntegerParameterValue(pathInfo.Version),
                new IntegerParameterValue((long)pathInfo.FileSize),
                new BlobParameterValue(contentsHash),
                new IntegerParameterValue(fileId)
            );
            Debug.Assert(rows == 1);
            db.Commit(false);

            return contentsHash;
        }

        /// <summary>Returns files which could not be linked because of 'too many links'</summary>
        private List<FileInfoRecord> TryHardLinkFiles(FileInfoRecord linkDestFileInfo, FileInfoRecord[] toLinkFileInfos) {
            var fileSize = linkDestFileInfo.FileSize;
            var linkDestPathContentsHash = GetOrCreateContentHash(linkDestFileInfo);
            var leftToLinkFileInfos = toLinkFileInfos.ToList();
            while (leftToLinkFileInfos.Any()) {
                var fi = leftToLinkFileInfos[^1];
                string path = fi.Path;
                var toLinkContentsHash = GetOrCreateContentHash(fi);
                bool sameContentsHash = 
                    linkDestPathContentsHash != null && 
                    toLinkContentsHash != null && 
                    linkDestPathContentsHash.SequenceEqual(toLinkContentsHash);
                if (!sameContentsHash) {
                    // A false positive; report and ignore
                    Console.WriteLine($"? '{path}': Files are (unexpectedly) different; not linking different files");
                } else if (conf.DoLink) {
                    string tmpPath = path + TmpFileExt;
                    File.Move(path, tmpPath);
                    bool ok = Win32Api.CreateHardLink(@"\\?\" + path, @"\\?\" + linkDestFileInfo.Path, IntPtr.Zero);
                    if (!ok) {
                        int err = Marshal.GetLastWin32Error();
                        if (err == 1142) {
                            // 1142: Too Many Links (ERROR_TOO_MANY_LINKS; 0x80070476).
                            File.Move(tmpPath, path); // Undo
                            break;
                        } else {
                            Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
                        }
                    }

                    if (!Safe($"while hard-linking '{path}'; undoing...", () => File.Delete(tmpPath))) {
                        Safe("while undoing; ignoring...", () => {
                            // Probably in use; undo operation...
                            File.Delete(path);
                            File.Move(tmpPath, path);
                        });
                    }
                }
                // Pop after success:
                leftToLinkFileInfos.RemoveAt(leftToLinkFileInfos.Count - 1); 
            }
            return leftToLinkFileInfos;
        }

        private static bool OptimalNrOfGroups(List<FileInfoRecord[]> groups) {
            int nrOfFiles = groups.Select(grp => grp.Length).Sum();
            int minNrOfGroups = (nrOfFiles + 1023) / 1024;              // Max. 1024 hardlinks to one file
            return groups.Count == minNrOfGroups;
        }

        private static List<FileInfoRecord[]>[] SplitFileGroupsBasedOnContent(
            Func<FileInfoRecord, byte[]> contentHashFunc, List<FileInfoRecord[]> fileInfoRecordGroups
        ) {
            // For each group with same size and version, assume contents is the same. Determine contents hash per group.
            var byContents = new Dictionary<byte[], List<FileInfoRecord[]>>(ByteArrayComparer.Singleton);
            foreach (var grp in fileInfoRecordGroups) {
                var contentsHash = grp.Select(contentHashFunc).FirstOrDefault(h => h != null);
                if (contentsHash == null) {
                    continue; // None of the files in group could be read -> ignore group
                }
                if (!byContents.TryGetValue(contentsHash, out var sameContent)) {
                    sameContent = byContents[contentsHash] = new();
                }
                sameContent.Add(grp);
            }
            return byContents.Values.ToArray();
        }

        private void HandleFilesWithSameSize(List<FileInfoRecord> sameSizeFileInfos) {
            stats.MaxNrOfFilesSameSize = Math.Max(stats.MaxNrOfFilesSameSize, sameSizeFileInfos.Count);
            var fi = sameSizeFileInfos[0];
            WriteProgress(() => $"{fi.FileSize}x{sameSizeFileInfos.Count}");
            var byVersion = sameSizeFileInfos.GroupBy(fi => fi.Version).Select(fis => fis.ToArray()).ToList();
            if (OptimalNrOfGroups(byVersion)) {
                return;  // Hardlinking will not create more space: all files already hardlinked optimally
            }

            // For each group with same size and version, assume contents is the same. Determine quickly sub-contents hash per group.
            var byQuickContents = SplitFileGroupsBasedOnContent(fi => CalculateQuickContentHash(fi.FileSize, fi.Path), byVersion);
            // Remove single-groups now:
            var byQuickContentsNonOptimized = byQuickContents.Where(g => !OptimalNrOfGroups(g)).ToArray();
            if (byQuickContents.Count() != byQuickContentsNonOptimized.Count()) {

            }

            // Now do it for the full file to be sure:
            var byContents = SplitFileGroupsBasedOnContent(fi => GetOrCreateContentHash(fi), byQuickContentsNonOptimized.SelectMany(x => x).ToList());

            // Iterate over groups of files with same content hash:
            foreach (var sameContents_ in byContents) {
                // Merge smallest group into largest group, so keep sorted:
                var sameContents = sameContents_.OrderByDescending(grp => grp.Length).ToList();

                if (!OptimalNrOfGroups(sameContents)) {
                    var fileSize = sameContents[0][0].FileSize;
                    var toLink = new[] { sameContents[0][0] }.Concat(sameContents.Skip(1).SelectMany(x => x)).Select(x => x.Path).ToArray();
                    Console.WriteLine($". Hard-linking {toLink.Length} files of {fileSize / 1024}Kb to:");
                    foreach (var path in toLink) {
                        Console.WriteLine($".    '{path}' ");
                    }
                }

                // While hardlinking might create more space...
                while (!OptimalNrOfGroups(sameContents)) {
                    // Biggest group of files (which are already linked) is used to link against:
                    var linkDestFileInfo = sameContents[0][0];
                    // Smallest group of files we try to 'hardlink' completely empty:
                    var toLinkFileInfos = sameContents[^1].ToArray();

                    var leftToLinkFileInfos = TryHardLinkFiles(linkDestFileInfo, toLinkFileInfos);
                    if (leftToLinkFileInfos.Any()) {
                        sameContents[^1] = leftToLinkFileInfos.ToArray();
                        // Not all where linked because of 'too many links', so link to next group:
                        sameContents.RemoveAt(0);
                    } else {
                        // Done with merging last group
                        sameContents.RemoveAt(sameContents.Count - 1);
                    }
                }
            }
        }

        private void Run(string[] args) {
            conf = Config.Parse(args);
            Win32Api.HandleFileInfo rootDirInfo = null;
            foreach (var rootDir in conf.RootDirs) {
                try {
                    var newRootDirInfo = Win32Api.GetPathInformation(rootDir, isDirectory: true);
                    if (rootDirInfo != null && rootDirInfo.VolumeSerialNumber != newRootDirInfo.VolumeSerialNumber) {
                        Console.WriteLine("! Directories are on different NTFS volumes; cannot perform hardlinking");
                        Environment.Exit(1);
                    }
                    rootDirInfo ??= newRootDirInfo;
                } catch (FileNotFoundException) {
                    Console.WriteLine($"! Could not find path '{rootDir}'");
                    Environment.Exit(1);
                }
            }

            string sqlPath = SqlPath(rootDirInfo.VolumeSerialNumber);
            Console.WriteLine($". Opening cache DB in '{Path.GetFullPath(sqlPath)}'...");
            db = new DatabaseConnection(sqlPath, false, new byte[] { 21, 3, 3, 2 });
            Console.WriteLine(". Initializing cache DB...");
            CreateSqlTables();
            foreach (string rootDir in conf.RootDirs) {
                IndexFilenames(rootDir);
            }
            foreach (var sameSizeFileInfos in ForAllFilesWithSameSize()) {
                // Do not hardlink when File extension differs (to speed up matching duplicates):
                foreach (var sameSizeAndExt in sameSizeFileInfos.GroupBy(fi => fi.Extension.ToLowerInvariant())) {
                    HandleFilesWithSameSize(sameSizeAndExt.ToList());
                }
            }

            Console.WriteLine($". Done: {stats}");
            db.Commit(true);
        }

        public static void Main(string[] args) => new Program().Run(args);
    }
}
