﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

#pragma warning disable CA1069 // Enums values should not be duplicated
#pragma warning disable RCS1135 // Declare enum member with zero value (when enum has FlagsAttribute).
#pragma warning disable RCS1234 // Duplicate enum value.
#pragma warning disable RCS1191 // Declare enum value as combination of names.
#pragma warning disable RCS1157
#pragma warning disable RCS1154
namespace MkDedupLinks {
    [Flags]
    internal enum EFileAccess : uint {
        AccessSystemSecurity = 0x1000000,   // AccessSystemAcl access type
        MaximumAllowed = 0x2000000,     // MaximumAllowed access type

        Delete = 0x10000,
        ReadControl = 0x20000,
        WriteDAC = 0x40000,
        WriteOwner = 0x80000,
        Synchronize = 0x100000,

        StandardRightsRequired = 0xF0000,
        StandardRightsRead = ReadControl,
        StandardRightsWrite = ReadControl,
        StandardRightsExecute = ReadControl,
        StandardRightsAll = 0x1F0000,
        SpecificRightsAll = 0xFFFF,

        FILE_READ_DATA = 0x0001,        // file & pipe
        FILE_LIST_DIRECTORY = 0x0001,       // directory
        FILE_WRITE_DATA = 0x0002,       // file & pipe
        FILE_ADD_FILE = 0x0002,         // directory
        FILE_APPEND_DATA = 0x0004,      // file
        FILE_ADD_SUBDIRECTORY = 0x0004,     // directory
        FILE_CREATE_PIPE_INSTANCE = 0x0004, // named pipe
        FILE_READ_EA = 0x0008,          // file & directory
        FILE_WRITE_EA = 0x0010,         // file & directory
        FILE_EXECUTE = 0x0020,          // file
        FILE_TRAVERSE = 0x0020,         // directory
        FILE_DELETE_CHILD = 0x0040,     // directory
        FILE_READ_ATTRIBUTES = 0x0080,      // all
        FILE_WRITE_ATTRIBUTES = 0x0100,     // all

        GenericRead = 0x80000000,
        GenericWrite = 0x40000000,
        GenericExecute = 0x20000000,
        GenericAll = 0x10000000,

        SPECIFIC_RIGHTS_ALL = 0x00FFFF,
        FILE_ALL_ACCESS = StandardRightsRequired | Synchronize | 0x1FF,
        FILE_GENERIC_READ = StandardRightsRead | FILE_READ_DATA | FILE_READ_ATTRIBUTES | FILE_READ_EA | Synchronize,
        FILE_GENERIC_WRITE = StandardRightsWrite | FILE_WRITE_DATA |FILE_WRITE_ATTRIBUTES | FILE_WRITE_EA | FILE_APPEND_DATA | Synchronize,
        FILE_GENERIC_EXECUTE = StandardRightsExecute | FILE_READ_ATTRIBUTES | FILE_EXECUTE | Synchronize
    }

    [Flags]
    public enum EFileShare : uint {
        None = 0x00000000,
        /// <summary>
        /// Enables subsequent open operations on an object to request read access.
        /// Otherwise, other processes cannot open the object if they request read access.
        /// If this flag is not specified, but the object has been opened for read access, the function fails.
        /// </summary>
        Read = 0x00000001,
        /// <summary>
        /// Enables subsequent open operations on an object to request write access.
        /// Otherwise, other processes cannot open the object if they request write access.
        /// If this flag is not specified, but the object has been opened for write access, the function fails.
        /// </summary>
        Write = 0x00000002,
        /// <summary>
        /// Enables subsequent open operations on an object to request delete access.
        /// Otherwise, other processes cannot open the object if they request delete access.
        /// If this flag is not specified, but the object has been opened for delete access, the function fails.
        /// </summary>
        Delete = 0x00000004
    }

    public enum ECreationDisposition : uint {
        /// <summary>
        /// Creates a new file. The function fails if a specified file exists.
        /// </summary>
        New = 1,
        /// <summary>
        /// Creates a new file, always.
        /// If a file exists, the function overwrites the file, clears the existing attributes, combines the specified file attributes,
        /// and flags with FILE_ATTRIBUTE_ARCHIVE, but does not set the security descriptor that the SECURITY_ATTRIBUTES structure specifies.
        /// </summary>
        CreateAlways = 2,
        /// <summary>
        /// Opens a file. The function fails if the file does not exist.
        /// </summary>
        OpenExisting = 3,
        /// <summary>
        /// Opens a file, always.
        /// If a file does not exist, the function creates a file as if dwCreationDisposition is CREATE_NEW.
        /// </summary>
        OpenAlways = 4,
        /// <summary>
        /// Opens a file and truncates it so that its size is 0 (zero) bytes. The function fails if the file does not exist.
        /// The calling process must open the file with the GENERIC_WRITE access right.
        /// </summary>
        TruncateExisting = 5
    }

    [Flags]
    public enum EFileAttributes : uint {
        Readonly = 0x00000001,
        Hidden = 0x00000002,
        System = 0x00000004,
        Directory = 0x00000010,
        Archive = 0x00000020,
        Device = 0x00000040,
        Normal = 0x00000080,
        Temporary = 0x00000100,
        SparseFile = 0x00000200,
        ReparsePoint = 0x00000400,
        Compressed = 0x00000800,
        Offline = 0x00001000,
        NotContentIndexed = 0x00002000,
        Encrypted = 0x00004000,
        FirstPipeInstance = 0x00080000,
        OpenNoRecall = 0x00100000,
        OpenReparsePoint = 0x00200000,
        PosixSemantics = 0x01000000,
        BackupSemantics = 0x02000000,
        DeleteOnClose = 0x04000000,
        SequentialScan = 0x08000000,
        RandomAccess = 0x10000000,
        NoBuffering = 0x20000000,
        Overlapped = 0x40000000,
        Write_Through = 0x80000000
    }

    public enum IfInvalidHandle { ThrowException, ReturnNull };

    public static class Win32Api {
        [StructLayout(LayoutKind.Explicit)]
        private struct LargeInteger {
            [FieldOffset(0)]
            public int Low;
            [FieldOffset(4)]
            public int High;
            [FieldOffset(0)]
            public long QuadPart;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        private struct WIN32_FIND_DATAW {
            public FileAttributes dwFileAttributes;
            internal FILETIME ftCreationTime;
            internal FILETIME ftLastAccessTime;
            internal FILETIME ftLastWriteTime;
            public uint nFileSizeHigh;
            public uint nFileSizeLow;
            public uint dwReserved0;
            public uint dwReserved1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string cFileName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
            public string cAlternateFileName;

            public ulong FileSize => ((ulong)nFileSizeHigh << 32) + nFileSizeLow;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private struct WIN32_FIND_DATA {
            public FileAttributes dwFileAttributes;
            public FILETIME ftCreationTime;
            public FILETIME ftLastAccessTime;
            public FILETIME ftLastWriteTime;
            public uint nFileSizeHigh;
            public uint nFileSizeLow;
            public uint dwReserved0;
            public uint dwReserved1;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
            public string cFileName;
            [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 14)]
            public string cAlternateFileName;

            public ulong FileSize => ((ulong)nFileSizeHigh << 32) + nFileSizeLow;
        }

        private struct BY_HANDLE_FILE_INFORMATION {
            public FileAttributes FileAttributes;
            public FILETIME CreationTime;
            public FILETIME LastAccessTime;
            public FILETIME LastWriteTime;
            public uint VolumeSerialNumber;
            public uint FileSizeHigh;
            public uint FileSizeLow;
            public uint NumberOfLinks;
            public uint FileIndexHigh;
            public uint FileIndexLow;

            public ulong CreationTimeAsUlong => ((ulong)CreationTime.dwHighDateTime << 32) + (uint)CreationTime.dwLowDateTime;
            public ulong LastWriteTimeAsUlong => ((ulong)LastWriteTime.dwHighDateTime << 32) + (uint)LastWriteTime.dwLowDateTime;
            public long Version =>
                ((long)(LastWriteTime.dwHighDateTime ^ CreationTime.dwLowDateTime) << 32) +
                (LastWriteTime.dwLowDateTime ^ CreationTime.dwHighDateTime);
            public ulong FileSize => ((ulong)FileSizeHigh << 32) + FileSizeLow;
            public ulong FileIndex => ((ulong)FileIndexHigh << 32) + FileIndexLow;
        }

        //private struct FILE_ID_BOTH_DIR_INFO {
        //    public uint NextEntryOffset;
        //    public uint FileIndex;
        //    public LargeInteger CreationTime;
        //    public LargeInteger LastAccessTime;
        //    public LargeInteger LastWriteTime;
        //    public LargeInteger ChangeTime;
        //    public LargeInteger EndOfFile;
        //    public LargeInteger AllocationSize;
        //    public uint FileAttributes;
        //    public uint FileNameLength;
        //    public uint EaSize;
        //    public char ShortNameLength;
        //    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 12)]
        //    public string ShortName;
        //    public LargeInteger FileId;
        //    [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
        //    public string FileName;
        //}

        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        private static extern IntPtr FindFirstFileW(string lpFileName, out WIN32_FIND_DATAW lpFindFileData);

        public const int FIND_FIRST_EX_CASE_SENSITIVE = 1;
        public const int FIND_FIRST_EX_LARGE_FETCH = 2;
        public const int FIND_FIRST_EX_ON_DISK_ENTRIES_ONLY = 4;

        public enum FINDEX_INFO_LEVELS { FindExInfoStandard = 0, FindExInfoBasic = 1 }

        public enum FINDEX_SEARCH_OPS {
            FindExSearchNameMatch = 0,
            FindExSearchLimitToDirectories = 1,
            FindExSearchLimitToDevices = 2
        }

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        private static extern IntPtr FindFirstFileEx(
            string lpFileName,
            FINDEX_INFO_LEVELS fInfoLevelId,
            out WIN32_FIND_DATAW lpFindFileData,
            FINDEX_SEARCH_OPS fSearchOp,
            IntPtr lpSearchFilter,
            int dwAdditionalFlags);
        
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode)]
        private static extern bool FindNextFileW(IntPtr hFindFile, out WIN32_FIND_DATAW lpFindFileData);

        [DllImport("kernel32.dll")]
        public static extern bool FindClose(IntPtr hFindFile);

        /// <summary>Converted information from WIN32_FIND_DATAW</summary>
        public record FindFileInfo(FileAttributes FileAttributes, FILETIME CreationTime, FILETIME LastWriteTime, ulong FileSize, string Path) {
            public bool IsDirectory => (FileAttributes & FileAttributes.Directory) != 0;
            public bool IsReparsePoint => (FileAttributes & FileAttributes.ReparsePoint) != 0;
            public ulong LastWriteTimeAsUlong => ((ulong)LastWriteTime.dwHighDateTime << 32) + (uint)LastWriteTime.dwLowDateTime;
            public long Version =>
                ((long)(LastWriteTime.dwHighDateTime ^ CreationTime.dwLowDateTime) << 32) +
                (LastWriteTime.dwLowDateTime ^ CreationTime.dwHighDateTime);
        }

        /// <summary>Converted information from BY_HANDLE_FILE_INFORMATION</summary>
        public record HandleFileInfo(FileAttributes FileAttributes, FILETIME CreationTime, FILETIME LastWriteTime, ulong FileSize, uint VolumeSerialNumber, ulong FileIndex) {
            public long Version =>
                ((long)(LastWriteTime.dwHighDateTime ^ CreationTime.dwLowDateTime) << 32) +
                (LastWriteTime.dwLowDateTime ^ CreationTime.dwHighDateTime);
        }

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool GetFileInformationByHandle(IntPtr hFile, out BY_HANDLE_FILE_INFORMATION lpFileInformation);

        //[DllImport("kernel32.dll", SetLastError = true)]
        //private static extern bool GetFileInformationByHandleEx(
        //    IntPtr hFile,
        //    int infoClass,
        //    ref FILE_ID_BOTH_DIR_INFO fileInfo,
        //    uint dwBufferSize);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool CloseHandle(IntPtr hObject);
        
        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern SafeFileHandle CreateFile(
                string fileName,
                [MarshalAs(UnmanagedType.U4)] FileAccess fileAccess,
                [MarshalAs(UnmanagedType.U4)] FileShare fileShare,
                IntPtr securityAttributes,
                [MarshalAs(UnmanagedType.U4)] FileMode creationDisposition,
                int flags,
                IntPtr template);

        private static readonly IntPtr INVALID_HANDLE_VALUE = new(-1);

        //[DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        //public static extern unsafe int GetFullPathName(string lpFileName, int nBufferLength, [Out, MarshalAs(UnmanagedType.LPTStr)] StringBuilder lpBuffer, [Out, MarshalAs(UnmanagedType.LPTStr)] StringBuilder lpFilePart);

        public static HandleFileInfo GetPathInformation(string ntfsPath, bool isDirectory, IfInvalidHandle ifInvalidHandle = IfInvalidHandle.ThrowException) {
            // To open a directory, 'BackupSemantics' is required:
            int attributes = isDirectory ? (int)EFileAttributes.BackupSemantics : 0;
            using var handle = CreateFile(@"\\?\"+ ntfsPath, FileAccess.Read, FileShare.ReadWrite, IntPtr.Zero, FileMode.Open, attributes, IntPtr.Zero);
            if (handle.IsInvalid) {
                if (ifInvalidHandle == IfInvalidHandle.ReturnNull) {
                    Console.WriteLine($"! Cannot get information on '{ntfsPath}'");
                    return null;
                }
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            }
            GetFileInformationByHandle(handle.DangerousGetHandle(), out var fi);
            return new HandleFileInfo(fi.FileAttributes, fi.CreationTime, fi.LastWriteTime, fi.FileSize, fi.VolumeSerialNumber, fi.FileIndex);
        }

        /// <summary>Returns all filenames and directory names in a given directory</summary>
        public static IEnumerable<FindFileInfo> GetFileInfos(string directory, bool includeDirectoryInPath) {
            //var findHandle = FindFirstFileW(directory + @"\*", out var findData);
            var findHandle = FindFirstFileEx(@"\\?\"+ directory + @"\*", FINDEX_INFO_LEVELS.FindExInfoBasic, out var findData, 0, IntPtr.Zero, FIND_FIRST_EX_LARGE_FETCH | FIND_FIRST_EX_ON_DISK_ENTRIES_ONLY);
            if (findHandle == INVALID_HANDLE_VALUE) {
                Console.WriteLine($"? Indexing: cannot index '{directory}'");
                yield break;
            }
            do {
                string basename = findData.cFileName;
                if (basename == "." || basename == "..") continue;
                if (includeDirectoryInPath) basename = MyPath.Combine(directory, basename);
                var res = new FindFileInfo(findData.dwFileAttributes, findData.ftCreationTime, findData.ftLastWriteTime, findData.FileSize, basename);
                // const int FileFullDirectoryInfo = 0xe;
                // TODO: for speedup, use GetFileInformationByHandleEx(FileFullDirectoryInfo) to have less calls
                yield return res;
            } while (FindNextFileW(findHandle, out findData));
            if (findHandle != INVALID_HANDLE_VALUE) FindClose(findHandle);
        }

        //public static IEnumerable<FindFileInfo> GetFileInfos2(string directory, bool includeDirectoryInPath) {
        //    const int FileIdBothDirectoryInfo = 0xa;
        //    using var handle = CreateFile(directory, FileAccess.Read, FileShare.ReadWrite, IntPtr.Zero, FileMode.Open, (int)EFileAttributes.BackupSemantics, IntPtr.Zero);
        //    if (handle.IsInvalid) {
        //        Console.WriteLine($"? Indexing: cannot index '{directory}'");
        //        yield break;
        //    }

        //    var fileinfo3 = new FILE_ID_BOTH_DIR_INFO();
        //    int size3 = Marshal.SizeOf(fileinfo3) + (1000 * 2);
        //    IntPtr p_fileInfo3 = Marshal.AllocHGlobal(size3);
        //    Marshal.StructureToPtr(fileinfo3, p_fileInfo3, false);
        //    var result = GetFileInformationByHandleEx(handle.DangerousGetHandle(), FileIdBothDirectoryInfo, ref fileinfo3, (uint)size3);
        //    var fn3 = new StringBuilder(1000);
        //    int len3 = GetFullPathName(fileinfo3.FileName, 1000, fn3, null);
        //}

        [DllImport("Kernel32.dll", CharSet = CharSet.Unicode, SetLastError =true)]
        public static extern bool CreateHardLink(
          string lpFileName,
          string lpExistingFileName,
          IntPtr lpSecurityAttributes
        );
    }
}
