﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace SqlDatabase
{
    /// <summary>Transaction management of a SQLite database connection</summary>
    public class DatabaseConnection : IDisposable {
        public long CommitCount { get; private set; }
        private SQLiteConnection Connection { get; set; }
        private SQLiteTransaction _transaction;
        private DateTime _lastCommitAt;

        private KeyValueSqlTable _kvStore;
        public KeyValueSqlTable KvStore {
            get {
                _kvStore ??= new KeyValueSqlTable(this);
                return _kvStore;
            }
        }

        private readonly TextWriter _fileSystemLock;

        /// <summary>Event fired just before we will SQL commit.</summary>
        public event EventHandler OnPreCommit;

        private readonly Dictionary<string, SQLiteCommand> _commandTextToCommandCache = new();

        public long LastInsertRowId() => Connection.LastInsertRowId;

        private void CloseTransaction() {
            if (_transaction != null) {
                _transaction.Commit();
                _transaction.Dispose();
                _transaction = null;
            }
        }

        /// <summary>Periodically write back to disk. If force is true, commits now.
        /// If force is false, rate limit the committing to once per 10s.</summary>
        public void Commit(bool force, bool verbose = false) {
            CommitCount += 1;
            if (!force) {
                double secondsSinceLastCommit = (DateTime.Now - _lastCommitAt).TotalSeconds;
                if (secondsSinceLastCommit < 10) {
                    return; // Rate-limited. Do not write now.
                }
            }

            OnPreCommit?.Invoke(this, EventArgs.Empty);

            CloseTransaction();

            if (verbose) Console.WriteLine($". Commit'ed force'd={force}");
            _lastCommitAt = DateTime.Now;
        }

        public void Dispose() {
            GC.SuppressFinalize(this);
            CloseConnection();

            // Release lock:
            _fileSystemLock.Dispose();
        }

        private SQLiteCommand CreateCachedCommand(bool openTransaction, string commandText, params ParameterValue[] parameters) {
            if (openTransaction) {
                // Automatically start a new transaction, if needed:
                _transaction ??= Connection.BeginTransaction();
            }

            bool exists = _commandTextToCommandCache.TryGetValue(commandText, out var cmd);
            var sqliteParams = parameters.Select(v => v.ToSqlite()).ToArray();
            if (!exists) {
                cmd = _commandTextToCommandCache[commandText] = new SQLiteCommand(commandText, Connection);
                foreach (var p in sqliteParams) {
                    cmd.Parameters.Add(p);
                }
            }
            for (int i = 0; i < sqliteParams.Length; i++) {
                Debug.Assert(cmd.Parameters[i].DbType == sqliteParams[i].DbType);
                cmd.Parameters[i].Value = sqliteParams[i].Value;
            }
            return cmd;
        }

        /// <summary>Executes a command with optional parameters, and returns a single result row</summary>
        public object[] ExecuteRow(string commandText, ParameterValue[] parameters, string[] fields) =>
            ExecuteRows(commandText, parameters, fields).First();

        /// <summary>Executes a command with optional parameters, and iterates over the resulting rows</summary>
        public IEnumerable<object[]> ExecuteRows(string commandText, ParameterValue[] parameters, string[] fields) {
            var sqlCmd = CreateCachedCommand(true, commandText, parameters);
            using var reader = sqlCmd.ExecuteReader();
            while (reader.Read()) {
                var res = fields.Select(field => {
                    var val = reader[field];
                    return val == DBNull.Value ? null : val;
                }).ToArray();
                yield return res;
            }
        }

        public IEnumerable<Tuple<T1, T2, T3>> ExecuteRows<T1, T2, T3>(string commandText, ParameterValue[] parameters, string[] fields) {
            Debug.Assert(fields.Length == 3);
            foreach (var line in ExecuteRows(commandText, parameters, fields)) {
                yield return Tuple.Create<T1, T2, T3>((T1)line[0], (T2)line[1], (T3)line[2]);
            }
        }

        public IEnumerable<Tuple<T1, T2, T3, T4>> ExecuteRows<T1, T2, T3, T4>(string commandText, ParameterValue[] parameters, string[] fields) {
            Debug.Assert(fields.Length == 4);
            foreach (var line in ExecuteRows(commandText, parameters, fields)) {
                yield return Tuple.Create<T1, T2, T3, T4>((T1)line[0], (T2)line[1], (T3)line[2], (T4)line[3]);
            }
        }

        /// <summary>Executes a command with optional parameters, and returns the single scalar result value</summary>
        public T ExecuteScalar<T>(string commandText, params ParameterValue[] parameters) {
            var cmd = CreateCachedCommand(true, commandText, parameters);
            object res = cmd.ExecuteScalar();
            if (res == DBNull.Value) res = null;
            return (T) res;
        }

        /// <summary>Executes a command with optional parameters and without any result. Returns number of rows affected.</summary>
        public int ExecuteNonQuery(string commandText, params ParameterValue[] parameters) {
            var cmd = CreateCachedCommand(true, commandText, parameters);
            return cmd.ExecuteNonQuery();
        }

        private void OpenConnection(string sqlFilename) {
            Connection = new SQLiteConnection($"Data Source={sqlFilename};Version=3;");
            Connection.Open();
            new SQLiteCommand("PRAGMA encoding='UTF-8';", Connection).ExecuteNonQuery();
            new SQLiteCommand("PRAGMA foreign_keys = ON;", Connection).ExecuteNonQuery();
            object res = new SQLiteCommand("PRAGMA foreign_keys;", Connection).ExecuteScalar();
            Debug.Assert(res.ToString() == "1");
        }

        private void CloseConnection() {
            _kvStore = null;
            _commandTextToCommandCache.Values.ToList().ForEach(a => a.Dispose());
            _commandTextToCommandCache.Clear();
            CloseTransaction();
            Connection?.Dispose();
            Connection = null;
            // Bug in SQLite: otherwise file will be kept open...
            GC.Collect();
        }

        /// <summary>Compacts the on-disk database</summary>
        public void Optimize() {
            CloseTransaction();
            Debug.Assert(_transaction == null);
            // Rebuild database outside transaction:
            new SQLiteCommand("VACUUM;", Connection).ExecuteScalar();
        }

        public DatabaseConnection(string sqlFilename, bool clearDatabase, byte[] dbSchemaVersion) {
            // Acquire lock:
            var fileStream = File.Open(sqlFilename + ".lock", FileMode.Create, FileAccess.Write, FileShare.Read);
            _fileSystemLock = new StreamWriter(fileStream);

            if (!File.Exists(sqlFilename)) clearDatabase = true;

            if (clearDatabase) {
                SQLiteConnection.CreateFile(sqlFilename);
            }
            OpenConnection(sqlFilename);

            while (true) {
                _lastCommitAt = DateTime.Now;

                if (clearDatabase) {
                    // Clear database. Removing the file is not possible since we have it open,
                    // and SQLite does not want to let it go, even after closing connection.
                    _kvStore = null;
                    CloseTransaction();
                    new SQLiteCommand(@"
                        PRAGMA writable_schema = 1;
                        delete from sqlite_master where type in ('table', 'index', 'trigger');
                        PRAGMA writable_schema = 0;
                        VACUUM;
                        PRAGMA INTEGRITY_CHECK;
                    ", Connection).ExecuteNonQuery();
                    KvStore["SchemaVersion"] = dbSchemaVersion;
                }

                // Check Schema Version:
                byte[] dbVersion = KvStore["SchemaVersion"];
                if (dbVersion != null && dbSchemaVersion.SequenceEqual(dbVersion)) {
                    break; // Version OK
                }

                Console.WriteLine("! Database incompatible.\nDatabase will be regenerated.");
                clearDatabase = true;

                // Retry...
            }
        }
    }
}