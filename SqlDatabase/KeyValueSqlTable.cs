﻿using System.Diagnostics;

namespace SqlDatabase {
    /// <summary>A dictionary from String to Byte[] as a persistent SQL-table</summary>
    public class KeyValueSqlTable {
        private readonly DatabaseConnection _dbConnection;
        private readonly string _tableName;

        private void CreateTableIfNotExists() {
            _dbConnection.ExecuteNonQuery($@"CREATE TABLE IF NOT EXISTS {_tableName} (
                key TEXT NOT NULL PRIMARY KEY,
                value BLOB
            );");
        }

        /// <summary>Sets/get a value by key. If key does not exist, returns null.</summary>
        public byte[] this[string key] {
            get => _dbConnection.ExecuteScalar<byte[]>(
                $@"SELECT VALUE FROM {_tableName} WHERE key=?;", 
                new StringParameterValue(key)
            );
            set {
                int updateRows = _dbConnection.ExecuteNonQuery(
                    $@"UPDATE {_tableName} SET value=? WHERE key=?;",
                    new BlobParameterValue(value), 
                    new StringParameterValue(key)
                );

                if (updateRows == 0) {
                    // Key does not exist
                    int insertedRows = _dbConnection.ExecuteNonQuery(
                        $@"INSERT INTO {_tableName}(key, value) values (?, ?);", 
                        new StringParameterValue(key),
                        new BlobParameterValue(value) 
                    );
                    Debug.Assert(insertedRows == 1);
                }
            }
        }

        public KeyValueSqlTable(DatabaseConnection dbConn, string tableName = "KeyValue") {
            _tableName = tableName;
            _dbConnection = dbConn;
            CreateTableIfNotExists();
        }
    }
}