﻿using System.Data;
using System.Data.SQLite;

namespace SqlDatabase {
    public enum ParameterType {
        Integer,
        String,
        Blob
    }

    public class IntegerParameterValue : ParameterValue {
        public readonly long? Value;
        public IntegerParameterValue(long? value) { Value = value; }
        internal override SQLiteParameter ToSqlite() => new(DbType.Int64, (object)Value);
    }

    public class StringParameterValue : ParameterValue {
        public readonly string Value;
        public StringParameterValue(string value) { Value = value; }
        internal override SQLiteParameter ToSqlite() => new(DbType.String, (object)Value);
    }

    public class BlobParameterValue : ParameterValue {
        public readonly byte[] Value;
        public BlobParameterValue(byte[] value) { Value = value; }
        internal override SQLiteParameter ToSqlite() => new(DbType.Binary, (object)Value);
    }

    /// <summary>A parameter with a name and a value of a specific type in the database</summary>
    public abstract class ParameterValue {
        public long? IntegerValue() => ((IntegerParameterValue)this).Value;
        public string StringValue() => ((StringParameterValue)this).Value;
        public byte[] BlobValue() => ((BlobParameterValue)this).Value;
        internal abstract SQLiteParameter ToSqlite();
    }
}