﻿using System;
using System.Diagnostics;
using SqlDatabase;

namespace SqlDatabaseTest {
    public static class Program {
        public static void Main() {
            Console.WriteLine("Starting");
            var db = new DatabaseConnection("SqlDatabase.tmp", true, new byte[] { 1, 2, 3 });

            db.ExecuteNonQuery(
                @"CREATE TABLE IF NOT EXISTS IndexedDirs (
                path_id INTEGER NOT NULL PRIMARY KEY, 
                path TEXT NOT NULL,
                level INTEGER NOT NULL,
                filenames BLOB,
                filenames_hash INTEGER,
                filenames_count INTEGER
            );");
            for (int i = 0; i < 2; i++) {
                int rows = db.ExecuteNonQuery(
                    "INSERT INTO IndexedDirs(path, level) VALUES (?, ?)",
                    new StringParameterValue("dir"),
                    new IntegerParameterValue(2)
                );
                Debug.Assert(rows == 1);
            }

            db.ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS ContentsNgrams (
                path_id INTEGER NOT NULL,  
                ngram INTEGER NOT NULL,
                PRIMARY KEY(path_id, ngram)
            ) WITHOUT ROWID;");

            for (int i = 0; i < 2; i++) {
                int addedNgramsRows = db.ExecuteNonQuery(
                    "INSERT INTO ContentsNgrams(path_id, ngram) VALUES (?, ?)",
                    new IntegerParameterValue(1),
                    new IntegerParameterValue(2 + i)
                );
                Debug.Assert(addedNgramsRows == 1);
            }

            db.Commit(false);
            db.Commit(true);
            Debug.Assert(db.CommitCount == 2);

            Console.WriteLine("Done");
        }
    }
}